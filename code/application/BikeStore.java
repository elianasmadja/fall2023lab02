//Eliana Smadja - 2233978
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        
        Bicycle[] bicycles = new Bicycle[4];

        bicycles[0] = new Bicycle("SuperFast", 6, 50.0);
        bicycles[1] = new Bicycle("Need For Speed", 6, 60.0);
        bicycles[2] = new Bicycle("Bikes For Life", 5, 45.0);
        bicycles[3] = new Bicycle("Bikathon", 3, 40.0);

        for(int i = 0; i < bicycles.length; i++){
            System.out.println(bicycles[i]);
        }


    }
}
