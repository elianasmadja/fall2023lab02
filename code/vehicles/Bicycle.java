//Eliana Smadja - 2233978
package vehicles;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //CONSTRUCTOR//
    public Bicycle(String manufacturer, int numGears, double maxSpeed){
            this.manufacturer = manufacturer;
            this.numberGears = numGears;
            this.maxSpeed = maxSpeed;
        }
    
    //GET METHODS//
    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    //TOSTRING METHOD//
    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + 
        this.numberGears + ", Max Speed: " + this.maxSpeed;
   }



}
